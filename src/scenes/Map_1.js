import Player from '../Sprites/Player'

export default class extends Phaser.Scene {
  constructor () {
    super({ key: 'Map_1' })
  }

  init() {
    //
  }

  create () {
    this.player = new Player(this, 0, 0)
  }

  update() {
    this.player.update()
  }
}