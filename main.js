import express from 'express';
import path from 'path';

express()
    .get('/', function(req, res) { res.sendFile(path.join(__dirname, './dist/index.html')); })
    .use('/', express.static(path.join(__dirname, './dist/')))
    .listen(process.env.PORT || 3000, () => console.log(`Listening on ${ process.env.PORT || 3000 }`));